# Generated by Django 2.1 on 2018-09-04 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0022_bug_closed'),
    ]

    operations = [
        migrations.AddField(
            model_name='bug',
            name='product',
            field=models.CharField(blank=True, help_text='Product used for the bug filing (e.g. DRI) or NULL if not applicable. To be filled automatically.', max_length=50, null=True),
        ),
    ]
