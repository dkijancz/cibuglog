#!/usr/bin/env python3

import argparse
import django
import time

django.setup()

from CIResults.models import Issue, IssueFilterAssociated  # noqa

# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-f", "--filters", dest="filters", action="store_true", default=False,
                    help="Recompute the statistics for filters (extremelly slow)")
parser.add_argument("--no-filters", dest="filters", action="store_false",
                    help="Do not recompute the statistics for filters (default)")

parser.add_argument("-i", "--issues", dest="issues", action="store_true", default=True,
                    help="Recompute the statistics for issues (default)")
parser.add_argument("--no-issues", dest="issues", action="store_false",
                    help="Do not recompute the statistics for issues")
args = parser.parse_args()

# Recompute
issues = Issue.objects.filter(archived_on=None)
ifas = IssueFilterAssociated.objects.filter(issue__in=issues, deleted_on=None)

msg = "Found {} active issues, and {} active filters associations"
print(msg.format(len(issues), len(ifas)))

if args.filters:
    start = time.time()
    for i, filter in enumerate(ifas):
        print("Updating filter association {}/{}".format(i + 1, len(ifas)), end='\r')
        filter.update_statistics()
    print("Filter association statistics updated in {:.2f} ms".format((time.time() - start) * 1000))

if args.issues:
    start = time.time()
    for i, issue in enumerate(issues):
        print("Updating Issue {}/{}".format(i + 1, len(issues)), end='\r')
        issue.update_statistics()
    print("Issue statistics updated in {:.2f} ms".format((time.time() - start) * 1000))
