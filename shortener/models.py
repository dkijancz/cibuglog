from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone
import hashlib


class Shortener(models.Model):
    shorthand = models.CharField(max_length=50, unique=True)
    full = models.TextField()

    added_on = models.DateTimeField(auto_now_add=True)
    last_accessed = models.DateTimeField(null=True, blank=True,
                                         help_text="Last time the object was accessed")

    @classmethod
    def get_or_create(cls, full):
        shorthand = hashlib.sha1(full.encode()).hexdigest()

        try:
            short_url = cls.objects.get(shorthand=shorthand)
            short_url.last_accessed = timezone.now()
            short_url.save()
            return short_url
        except ObjectDoesNotExist:
            return cls.objects.create(full=full, shorthand=shorthand)

    def __str__(self):
        return "Short '{}'".format(self.shorthand)
